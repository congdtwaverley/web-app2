import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

export const authCodeFlowConfig: AuthConfig = {
  // Url of the Identity Provider
  issuer: environment.oauthconfig.issuer,

  // URL of the SPA to redirect the user to after login
  redirectUri: environment.oauthconfig.redirectUri,

  // The SPA's id. The SPA is registerd with this id at the auth-server
  // clientId: 'server.code',
  clientId: environment.oauthconfig.clientId,

  // Just needed if your auth server demands a secret. In general, this
  // is a sign that the auth server is not configured with SPAs in mind
  // and it might not enforce further best practices vital for security
  // such applications.
  // dummyClientSecret: 'secret',

  responseType: 'code',

  // set the scope for the permissions the client should request
  // The first four are defined by OIDC.
  // Important: Request offline_access to get a refresh token
  // The api scope is a usecase specific one
  scope: environment.oauthconfig.scope,

  requireHttps: environment.oauthconfig.requireHttps,

  showDebugInformation: environment.oauthconfig.showDebugInformation,
};

export const WELCOME_COMPONENT_ROLE_CODE: string = 'WELCOME';
export const HOME_COMPONENT_ROLE_CODE: string = 'HOME';

export const COMPONENT_ROLES: Record<string, string[]> = 
{
  WELCOME_COMPONENT_ROLE_CODE : [],
  HOME_COMPONENT_ROLE_CODE : ['webapp2_admin', 'webapp2_user']
};