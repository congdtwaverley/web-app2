import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WELCOME_COMPONENT_ROLE_CODE, HOME_COMPONENT_ROLE_CODE} from 'src/app/services/oauth2.service';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from './components/home/home.component';
import { WelcomeComponent } from './components/welcome/welcome.component';

const routes: Routes = [
  { path: "welcome", component: WelcomeComponent},
  { path: "home", component: HomeComponent, canActivate: [AuthGuard],
    data: {
      componentCode: HOME_COMPONENT_ROLE_CODE
    }},
  { path: "", redirectTo: "welcome", pathMatch: "full"},
  { path: "**", redirectTo: "welcome", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
