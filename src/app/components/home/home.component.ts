import { Component, OnInit } from '@angular/core';
import { Oauth2Service } from 'src/app/services/oauth2.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  userId?: string = "";
  userName?: string = "";
  nameOfUser?: string = "";
  userEmail?: string = "";
  taskTotal: number = 0;
  userRoles: string[] | undefined = [];

  constructor(private oauth2Service: Oauth2Service) {
   
  }

  ngOnInit(): void {
    if (this.oauth2Service.getUserInfo() != undefined) {
      this.userId = this.oauth2Service.getUserInfo()?.id;
      this.userName = this.oauth2Service.getUserInfo()?.userName;
      this.nameOfUser = this.oauth2Service.getUserInfo()?.nameOfUser;
      this.userEmail = this.oauth2Service.getUserInfo()?.userEmail;
      this.userRoles = this.oauth2Service.getUserRoles();
    }
  }

}
