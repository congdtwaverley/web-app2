import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Oauth2Service } from 'src/app/services/oauth2.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent{
  constructor(private oauth2Service: Oauth2Service) {
    
  }

  login() {
    this.oauth2Service.login();
  }

  hasLoginIn(): boolean {
    return this.oauth2Service.hasLoggedIn();
  }

}
