import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { authCodeFlowConfig} from '../oauth2-config';
import { UserInfo } from './sync-user-info';
import { JwtHelperService } from '@auth0/angular-jwt';


// import { ReadPropExpr } from '@angular/compiler';


export const WELCOME_COMPONENT_ROLE_CODE: string = 'WELCOME';
export const HOME_COMPONENT_ROLE_CODE: string = 'HOME';

export const COMPONENT_ROLES: Map<string, string[]> = new Map([
    [WELCOME_COMPONENT_ROLE_CODE, []], 
    [HOME_COMPONENT_ROLE_CODE, ['webapp2_admin', 'webapp2_user']]
  ]);

export const COMPONENT_ROLES_CODE: Set<string> = new Set([WELCOME_COMPONENT_ROLE_CODE, HOME_COMPONENT_ROLE_CODE]);

@Injectable({
  providedIn: 'root'
})
export class Oauth2Service {
  private isLoggedIn: boolean = false;
  private authUserInfo: UserInfo | undefined;
  private userRoles: string[] | undefined;
  
  private componentsAuth = new Map();

  constructor(private oauthService: OAuthService, private router: Router, private jwtHelper: JwtHelperService) {
    this.initComponentsAuth();
    this.configure();
  }

  initComponentsAuth() {
    this.componentsAuth.set(WELCOME_COMPONENT_ROLE_CODE, true);
    this.componentsAuth.set(HOME_COMPONENT_ROLE_CODE, false);
  }

  configure() {
    this.oauthService.configure(authCodeFlowConfig);
    this.oauthService.setupAutomaticSilentRefresh();
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    
    this.oauthService.loadDiscoveryDocumentAndTryLogin().then(() => {
      if(this.oauthService.hasValidAccessToken() && this.oauthService.hasValidIdToken()){
        this.isLoggedIn = true;
        const claims = this.oauthService.getIdentityClaims() as Record<string, any>;

        this.authUserInfo = {isSynced: false, id: claims['sub'], userName: claims['preferred_username']
        , nameOfUser: claims['name'], userEmail: claims['email'], firstName: claims['given_name'], lastName: claims['family_name']};
        
        this.oauthService.setupAutomaticSilentRefresh();
        var accessToken = this.oauthService.getAccessToken();
        console.log('accessToken is:' + accessToken);
       
        var decodedAccessToken = this.jwtHelper.decodeToken(accessToken); 
        var roles: string[] = decodedAccessToken['resource_access'][authCodeFlowConfig.clientId? authCodeFlowConfig.clientId: ""]['roles'];
        this.userRoles = roles;
        console.log('roles is:' + this.userRoles);
        this.setComponentsAuth(this.userRoles);
        
        for (var [key,value] of this.componentsAuth) {
          console.log('rolescomponentsAuth is:' + key + ' '+ value);
        }

        this.router.navigateByUrl('/home');
      }
    });
  }

  isComponentAccesssible(componentCode: string): boolean {
    return this.componentsAuth.get(componentCode);
  }

  setComponentsAuth(roles: string[]) {
    COMPONENT_ROLES_CODE.forEach(rc => {
      console.log('START SET COMPONENTS AUTH:' + rc)
      
      var accessibleRoles: string[] | undefined = COMPONENT_ROLES.get(rc);

      console.log('accessibleRole for component:' + rc + '|' +accessibleRoles)
      if (accessibleRoles == undefined || accessibleRoles.length == 0 ) {
        console.log('componentsAuth is:' + this.componentsAuth.get(rc));
        console.log('set auth case 1:' + this.componentsAuth.get(rc));
        this.componentsAuth.set(rc, true);
      } else {
        accessibleRoles.forEach(r => {
          if (roles.includes(r)) {
            console.log('componentsAuth is:' + this.componentsAuth.get(rc));
                console.log('set auth case 2:' + this.componentsAuth.get(rc));
            this.componentsAuth.set(rc, true);
          }
        })
      }
    })
  }

  getUserRoles(): string[] | undefined {
    return this.userRoles;
  }

  login() {
    this.oauthService.initCodeFlow();
  }

  logout() {
    console.log('Logout URL:' + this.oauthService.logoutUrl)
    this.oauthService.revokeTokenAndLogout();
  }

  hasLoggedIn(): boolean {
    return this.isLoggedIn;
  }

  getUserInfo() {
    return this.authUserInfo;
  }

}
