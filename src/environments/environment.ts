export const environment = {
    production: false,
    oauthconfig: {
        issuer: "http://localhost:8080/realms/WaverleyEco",
        redirectUri: "http://localhost:4201",
        clientId: "web-app2",
        scope: "profile email offline_access phone",
        showDebugInformation: true,
        requireHttps: false,
        postLogoutRedirectUri: "http://localhost:4201"
    },
    backendUrlConfig: {
        rootURL: "http://localhost:8081"
    }
}
