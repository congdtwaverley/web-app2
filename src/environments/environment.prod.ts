export const environment = {
    production: false,
    oauthconfig: {
        issuer: "https://ec2-13-214-159-170.ap-southeast-1.compute.amazonaws.com:8443/realms/WaverleyEco",
        redirectUri: "http://ec2-13-213-6-169.ap-southeast-1.compute.amazonaws.com",
        clientId: "web-app1",
        scope: "profile email offline_access phone",
        showDebugInformation: true,
        requireHttps: true,
        postLogoutRedirectUri: "http://ec2-13-213-6-169.ap-southeast-1.compute.amazonaws.com"
    },
    backendUrlConfig: {
        rootURL: "http://ec2-18-136-105-192.ap-southeast-1.compute.amazonaws.com:8081"
    }
}